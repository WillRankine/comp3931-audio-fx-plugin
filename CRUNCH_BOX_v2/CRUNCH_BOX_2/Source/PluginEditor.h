/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"

struct CustomRotarySlider : juce::Slider {
    CustomRotarySlider() : juce::Slider(juce::Slider::SliderStyle::RotaryHorizontalVerticalDrag,
        juce::Slider::TextEntryBoxPosition::TextBoxBelow) //TODO Change to add text box
    {

    }
};

//==============================================================================
/**
*/
class CRUNCH_BOX_2AudioProcessorEditor  : public juce::AudioProcessorEditor
{
public:
    CRUNCH_BOX_2AudioProcessorEditor (CRUNCH_BOX_2AudioProcessor&);
    ~CRUNCH_BOX_2AudioProcessorEditor() override;

    //==============================================================================
    void paint (juce::Graphics&) override;
    void resized() override;
    
private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    CRUNCH_BOX_2AudioProcessor& audioProcessor;

    CustomRotarySlider noiseSlider,
        bitsSlider,
        rateSlider,
        reduxMixSlider,
        driveSlider,
        rangeSlider,
        hpfSlider,
        lpfSlider,
        volumeSlider,
        mixSlider;
    juce::Label noiseSliderLabel,
        bitsSliderLabel,
        rateSliderLabel,
        reduxMixSliderLabel,
        driveSliderLabel,
        rangeSliderLabel,
        hpfSliderLabel,
        lpfSliderLabel,
        volumeSliderLabel,
        mixSliderLabel;

    using APVTS = juce::AudioProcessorValueTreeState;
    using Attachment = APVTS::SliderAttachment;

    Attachment noiseSliderAttachment,
        bitsSliderAttachment,
        rateSliderAttachment,
        reduxMixSliderAttachment,
        driveSliderAttachment,
        rangeSliderAttachment,
        hpfSliderAttachment,
        lpfSliderAttachment,
        volumeSliderAttachment,
        mixSliderAttachment;
    
    std::vector<juce::Component*> getComps();

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (CRUNCH_BOX_2AudioProcessorEditor)
};
