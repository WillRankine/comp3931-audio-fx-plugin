/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

enum Slope
{
    Slope_12,
    Slope_24,
    Slope_36,
    Slope_48
};

// Struct to store plugin paramater settings
struct paramaterSettings
{
    float noise{ 0 }, bits{ 16.f }, rate{ 1.f }, reduxMix{ 1.f }, drive{ 0 };
    float range{ 0 }, hpf{ 20.f }, lpf{ 20000.f }, volume{ 1.f }, mix{ 1.f };
};

paramaterSettings getParamaterSettings(juce::AudioProcessorValueTreeState& apvts);

//==============================================================================
/**
*/
class CRUNCH_BOX_2AudioProcessor  : public juce::AudioProcessor
{
public:
    //==============================================================================
    CRUNCH_BOX_2AudioProcessor();
    ~CRUNCH_BOX_2AudioProcessor() override;

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

   #ifndef JucePlugin_PreferredChannelConfigurations
    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;
   #endif

    void processBlock (juce::AudioBuffer<float>&, juce::MidiBuffer&) override;

    //==============================================================================
    juce::AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const juce::String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const juce::String getProgramName (int index) override;
    void changeProgramName (int index, const juce::String& newName) override;

    //==============================================================================
    void getStateInformation (juce::MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;

    // The APVTS requires a list of all paramaters before it starts, this function creates that paramater layout
    static juce::AudioProcessorValueTreeState::ParameterLayout createParameterLayout();
    // Create an AudioProcessorValueTreeState to hold the values of all the paramaters in the plugin
    // That audio processor is *this, no undo manager is used (nullptr), the identifier for the apvts is "Paramaters"
    // 
    juce::AudioProcessorValueTreeState apvts{ *this, nullptr, "Parameters", createParameterLayout() };

    juce::AudioVisualiserComponent waveViewer;

private:

    using Filter = juce::dsp::IIR::Filter<float>;
    
    using CutFilter = juce::dsp::ProcessorChain<Filter, Filter, Filter, Filter>;

    using MonoChain = juce::dsp::ProcessorChain<CutFilter, Filter, CutFilter>;

    MonoChain leftChain, rightChain;

    juce::dsp::DryWetMixer<float> leftMixer;
    juce::dsp::DryWetMixer<float> rightMixer;


    enum ChainPositions
    {
        HighCut,
        Peak,
        LowCut
    };

    using Coefficients = Filter::CoefficientsPtr;
    static void updateCoefficients(Coefficients& old, const Coefficients& replacements);

    /*template<typename ChainType, typename CoefficientType>
    void updateCutFilter(ChainType& leftLowCut,
        const CoefficientType& cutCoefficients,
        const ChainSettings& chainSettings)
    {

    }*/

    juce::Random random;


    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (CRUNCH_BOX_2AudioProcessor)
};
