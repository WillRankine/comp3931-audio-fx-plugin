/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
CRUNCH_BOX_2AudioProcessorEditor::CRUNCH_BOX_2AudioProcessorEditor(CRUNCH_BOX_2AudioProcessor& p)
    : AudioProcessorEditor(&p), audioProcessor(p),
    noiseSliderAttachment(audioProcessor.apvts, "NoiseLevel", noiseSlider),
    bitsSliderAttachment(audioProcessor.apvts, "Bits", bitsSlider),
    rateSliderAttachment(audioProcessor.apvts, "Rate", rateSlider),
    reduxMixSliderAttachment(audioProcessor.apvts, "ReduxMix", reduxMixSlider),
    driveSliderAttachment(audioProcessor.apvts, "Drive", driveSlider),
    rangeSliderAttachment(audioProcessor.apvts, "Range", rangeSlider),
    hpfSliderAttachment(audioProcessor.apvts, "HPF", hpfSlider),
    lpfSliderAttachment(audioProcessor.apvts, "LPF", lpfSlider),
    volumeSliderAttachment(audioProcessor.apvts, "Volume", volumeSlider),
    mixSliderAttachment(audioProcessor.apvts, "Mix", mixSlider)
       
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.

    addAndMakeVisible(audioProcessor.waveViewer);
    audioProcessor.waveViewer.setColours(juce::Colours::black, juce::Colours::whitesmoke.withAlpha(0.5f));


    
    for (auto* comp : getComps())
    {
        addAndMakeVisible(comp);
    }

    setSize (600, 400);

    
}

CRUNCH_BOX_2AudioProcessorEditor::~CRUNCH_BOX_2AudioProcessorEditor()
{
}

//==============================================================================
void CRUNCH_BOX_2AudioProcessorEditor::paint (juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId));

    g.setColour (juce::Colours::white);
    g.setFont (15.0f);
   // g.drawFittedText ("Hello World!", getLocalBounds(), juce::Justification::centred, 1);
}

void CRUNCH_BOX_2AudioProcessorEditor::resized()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..

    auto bounds = getLocalBounds();
    auto full_waveViewerArea = bounds.removeFromBottom(bounds.getHeight() * 0.33);
    auto left_waveViewerArea = full_waveViewerArea.removeFromLeft(full_waveViewerArea.getWidth() * 0.95);
    auto right_waveViewerArea = left_waveViewerArea.removeFromRight(left_waveViewerArea.getWidth() * 0.95);
    auto top_waveViewerArea = right_waveViewerArea.removeFromTop(right_waveViewerArea.getHeight() * 0.95);
    auto waveViewerArea = top_waveViewerArea.removeFromBottom(top_waveViewerArea.getHeight() * 0.95);

    //auto noiseArea = bounds.removeFromLeft
    auto noise_bitReduxArea = bounds.removeFromLeft(bounds.getWidth() * 0.33);

    
    auto distortionArea = bounds.removeFromLeft(bounds.getWidth() * 0.5);
    
    auto filter_vol_mixArea = bounds.removeFromLeft(bounds.getWidth());

    auto noiseArea = noise_bitReduxArea.removeFromLeft(noise_bitReduxArea.getWidth() * 0.5);
    noiseSliderLabel.setBounds(noiseArea.removeFromTop(noiseArea.getHeight() * 0.1));
    noiseSlider.setBounds(noiseArea);
    noiseSliderLabel.setText("Noise", juce::dontSendNotification);
    noiseSliderLabel.setJustificationType(juce::Justification::centred);
    //noiseSliderLabel.attachToComponent(&noiseSlider, false);
    

    bitsSliderLabel.setBounds(noise_bitReduxArea.removeFromTop(noise_bitReduxArea.getHeight() * 0.1));
    bitsSlider.setBounds(noise_bitReduxArea.removeFromTop(noise_bitReduxArea.getHeight() * 0.5));
    bitsSliderLabel.setText("Bits", juce::dontSendNotification);
    bitsSliderLabel.setJustificationType(juce::Justification::centred);
    //bitsSliderLabel.attachToComponent(&bitsSlider, false);

    reduxMixSliderLabel.setBounds(noise_bitReduxArea.removeFromTop(noise_bitReduxArea.getHeight() * 0.3));
    reduxMixSlider.setBounds(noise_bitReduxArea);
    reduxMixSliderLabel.setText("Redux-Mix", juce::dontSendNotification);
    reduxMixSliderLabel.setJustificationType(juce::Justification::centred);
    //reduxMixSliderLabel.attachToComponent(&reduxMixSlider, false);


    driveSliderLabel.setBounds(distortionArea.removeFromTop(distortionArea.getHeight() * 0.1));
    driveSlider.setBounds(distortionArea.removeFromTop(distortionArea.getHeight()*0.56));
    driveSliderLabel.setText("Drive", juce::dontSendNotification);
    driveSliderLabel.setJustificationType(juce::Justification::centred);
    //driveSliderLabel.attachToComponent(&driveSlider, false);

    rangeSliderLabel.setBounds(distortionArea.removeFromTop(distortionArea.getHeight() * 0.2));
    rangeSlider.setBounds(distortionArea);
    rangeSliderLabel.setText("Range", juce::dontSendNotification);
    rangeSliderLabel.setJustificationType(juce::Justification::centred);
    //rangeSliderLabel.attachToComponent(&rangeSlider, false);


    auto filterArea = filter_vol_mixArea.removeFromLeft(filter_vol_mixArea.getWidth() * 0.5);

    lpfSliderLabel.setBounds(filterArea.removeFromTop(filterArea.getHeight() * 0.1));
    lpfSlider.setBounds(filterArea.removeFromTop(filterArea.getHeight() * 0.4));
    lpfSliderLabel.setText("LPF", juce::dontSendNotification);
    //lpfSliderLabel.attachToComponent(&lpfSlider, false);
    lpfSliderLabel.setJustificationType(juce::Justification::centred);
    lpfSlider.setTextValueSuffix(" Hz");


    volumeSliderLabel.setBounds(filterArea.removeFromTop(filterArea.getHeight() * 0.2));
    volumeSlider.setBounds(filterArea);
    volumeSliderLabel.setText("Volume", juce::dontSendNotification);
    //hpfSliderLabel.attachToComponent(&hpfSlider, false);
    
    hpfSliderLabel.setJustificationType(juce::Justification::centred);
    hpfSlider.setTextValueSuffix(" Hz");


    hpfSliderLabel.setBounds(filter_vol_mixArea.removeFromTop(filter_vol_mixArea.getHeight() * 0.1));
    hpfSlider.setBounds(filter_vol_mixArea.removeFromTop(filter_vol_mixArea.getHeight() * 0.4));
    hpfSliderLabel.setText("HPF", juce::dontSendNotification);
    //volumeSliderLabel.attachToComponent(&volumeSlider, false);
    volumeSliderLabel.setJustificationType(juce::Justification::centred);


    mixSliderLabel.setBounds(filter_vol_mixArea.removeFromTop(filter_vol_mixArea.getHeight() * 0.2));
    mixSlider.setBounds(filter_vol_mixArea);
    mixSliderLabel.setText("Mix", juce::dontSendNotification);
    //mixSliderLabel.attachToComponent(&mixSlider, false);
    mixSliderLabel.setJustificationType(juce::Justification::centred);


    //auto noiseArea = bounds.removeFromLeft(noise_bitReduxArea.getWidth() * 0.5);


    //auto reduxArea = bounds.removeFromLeft(noise_bitReduxArea.getWidth());
    
    //noiseSlider.setBounds(noiseArea);
    //bitsSlider.setBounds(bounds.removeFromTop(bounds.getHeight() * 0.5));
    //reduxMixSlider.setBounds(bounds);

    audioProcessor.waveViewer.setBounds(waveViewerArea);

    

    //audioProcessor.waveViewer.setBounds(getLocalBounds().withSizeKeepingCentre(getWidth() * 0.5, getHeight() * 0.5));
}

std::vector<juce::Component*> CRUNCH_BOX_2AudioProcessorEditor::getComps() 
{
    return
    {
        &noiseSlider,
        &bitsSlider,
        &rateSlider,
        &reduxMixSlider,
        &driveSlider,
        &rangeSlider,
        &hpfSlider,
        &lpfSlider,
        &volumeSlider,
        &mixSlider,
        &noiseSliderLabel,
        &bitsSliderLabel,
        &rateSliderLabel,
        &reduxMixSliderLabel,
        &driveSliderLabel,
        &rangeSliderLabel,
        &hpfSliderLabel,
        &lpfSliderLabel,
        &volumeSliderLabel,
        &mixSliderLabel
    };
}