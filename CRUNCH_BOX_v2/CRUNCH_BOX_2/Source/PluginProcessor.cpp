/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
CRUNCH_BOX_2AudioProcessor::CRUNCH_BOX_2AudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  juce::AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", juce::AudioChannelSet::stereo(), true)
                     #endif
                       ), waveViewer(1)
#endif
{
    waveViewer.setRepaintRate(30);
    waveViewer.setBufferSize(256);
}

CRUNCH_BOX_2AudioProcessor::~CRUNCH_BOX_2AudioProcessor()
{
}

//==============================================================================
const juce::String CRUNCH_BOX_2AudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool CRUNCH_BOX_2AudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool CRUNCH_BOX_2AudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool CRUNCH_BOX_2AudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double CRUNCH_BOX_2AudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int CRUNCH_BOX_2AudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int CRUNCH_BOX_2AudioProcessor::getCurrentProgram()
{
    return 0;
}

void CRUNCH_BOX_2AudioProcessor::setCurrentProgram (int index)
{
}

const juce::String CRUNCH_BOX_2AudioProcessor::getProgramName (int index)
{
    return {};
}

void CRUNCH_BOX_2AudioProcessor::changeProgramName (int index, const juce::String& newName)
{
}

//==============================================================================
void CRUNCH_BOX_2AudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    juce::dsp::ProcessSpec spec;
    spec.maximumBlockSize = samplesPerBlock;

    spec.numChannels = 1;

    spec.sampleRate = sampleRate;

    // Create a chain for each channel of audio
    leftChain.prepare(spec);
    rightChain.prepare(spec);

    leftMixer.prepare(spec);
    rightMixer.prepare(spec);

    auto paramaterSettings = getParamaterSettings(apvts);

    // HIGH PASS
    //leftChain.get<ChainPositions::Peak // look at min 50 of video
    float highPassSlope = 0; // 12db per octive slope (can change later) (min 1:00:22 of vid)

    auto lowCutCoefficients = juce::dsp::FilterDesign<float>::designIIRHighpassHighOrderButterworthMethod(
        paramaterSettings.hpf, sampleRate, 2 * (highPassSlope + 1));

    // Left High pass
    auto& leftHighPass = leftChain.get<ChainPositions::LowCut>();

    leftHighPass.setBypassed<0>(true);
    leftHighPass.setBypassed<1>(true);
    leftHighPass.setBypassed<2>(true);
    leftHighPass.setBypassed<3>(true);

    *leftHighPass.get<0>().coefficients = *lowCutCoefficients[0];
    leftHighPass.setBypassed<0>(false);

    // Right High pass
    auto& rightHighPass = rightChain.get<ChainPositions::LowCut>();

    rightHighPass.setBypassed<0>(true);
    rightHighPass.setBypassed<1>(true);
    rightHighPass.setBypassed<2>(true);
    rightHighPass.setBypassed<3>(true);

    *rightHighPass.get<0>().coefficients = *lowCutCoefficients[0];
    rightHighPass.setBypassed<0>(false);

    // LOW PASS

    float lowPassSlope = 0;

    auto highCutCoefficients = juce::dsp::FilterDesign<float>::designIIRLowpassHighOrderButterworthMethod(
        paramaterSettings.lpf, sampleRate, 2 * (lowPassSlope + 1));

    // Left Low pass
    auto& leftLowPass = leftChain.get<ChainPositions::HighCut>();

    leftLowPass.setBypassed<0>(true);
    leftLowPass.setBypassed<1>(true);
    leftLowPass.setBypassed<2>(true);
    leftLowPass.setBypassed<3>(true);

    *leftLowPass.get<0>().coefficients = *highCutCoefficients[0];
    leftLowPass.setBypassed<0>(false);

    // Right Low pass
    auto& rightLowPass = rightChain.get<ChainPositions::HighCut>();

    rightLowPass.setBypassed<0>(true);
    rightLowPass.setBypassed<1>(true);
    rightLowPass.setBypassed<2>(true);
    rightLowPass.setBypassed<3>(true);

    *rightLowPass.get<0>().coefficients = *highCutCoefficients[0];
    rightLowPass.setBypassed<0>(false);

    leftMixer.setWetMixProportion(paramaterSettings.mix);
    rightMixer.setWetMixProportion(paramaterSettings.mix);


}

void CRUNCH_BOX_2AudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool CRUNCH_BOX_2AudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    juce::ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    // Some plugin hosts, such as certain GarageBand versions, will only
    // load plugins that support stereo bus layouts.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void CRUNCH_BOX_2AudioProcessor::processBlock (juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    

    juce::ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();

    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels.
    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());
    auto paramaterSettings = getParamaterSettings(apvts);

    
    // HIGH PASS
    float highPassSlope = 0;
    auto cutCoefficients = juce::dsp::FilterDesign<float>::designIIRHighpassHighOrderButterworthMethod(
        paramaterSettings.hpf, getSampleRate(), 2 * (highPassSlope + 1));

    // Left High pass
    auto& leftHighPass = leftChain.get<ChainPositions::LowCut>();

    leftHighPass.setBypassed<0>(true);
    leftHighPass.setBypassed<1>(true);
    leftHighPass.setBypassed<2>(true);
    leftHighPass.setBypassed<3>(true);

    *leftHighPass.get<0>().coefficients = *cutCoefficients[0];
    leftHighPass.setBypassed<0>(false);

    // Right High pass
    auto& rightHighPass = rightChain.get<ChainPositions::LowCut>();

    rightHighPass.setBypassed<0>(true);
    rightHighPass.setBypassed<1>(true);
    rightHighPass.setBypassed<2>(true);
    rightHighPass.setBypassed<3>(true);

    *rightHighPass.get<0>().coefficients = *cutCoefficients[0];
    rightHighPass.setBypassed<0>(false);

    // LOW PASS

    float lowPassSlope = 0;

    auto highCutCoefficients = juce::dsp::FilterDesign<float>::designIIRLowpassHighOrderButterworthMethod(
        paramaterSettings.lpf, getSampleRate(), 2 * (lowPassSlope + 1));

    // Left Low pass
    auto& leftLowPass = leftChain.get<ChainPositions::HighCut>();

    leftLowPass.setBypassed<0>(true);
    leftLowPass.setBypassed<1>(true);
    leftLowPass.setBypassed<2>(true);
    leftLowPass.setBypassed<3>(true);

    *leftLowPass.get<0>().coefficients = *highCutCoefficients[0];
    leftLowPass.setBypassed<0>(false);

    // Right Low pass
    auto& rightLowPass = rightChain.get<ChainPositions::HighCut>();

    rightLowPass.setBypassed<0>(true);
    rightLowPass.setBypassed<1>(true);
    rightLowPass.setBypassed<2>(true);
    rightLowPass.setBypassed<3>(true);

    *rightLowPass.get<0>().coefficients = *highCutCoefficients[0];
    rightLowPass.setBypassed<0>(false);

    // Get redux rate divide
    auto rateDivide = static_cast<int> (paramaterSettings.rate);

    // process distortion, bit reduction and white nosie
    for (int channel = 0; channel < totalNumInputChannels; ++channel)
    {
        auto* channelData = buffer.getWritePointer (channel);

        // for each sample in buffer
        for (int sample = 0; sample < buffer.getNumSamples(); sample++) {

            // generate noise signal
            auto noise_signal = random.nextFloat() * 2.0f - 1.0f;


            // preserve original signal
            float cleanSig = *channelData;

            // reduce bit rate according to slider positions
            float totalQLevels = powf(2, paramaterSettings.bits); // round bit depth to power of 2
            float val = *channelData;
            float remainder = fmodf(val, 1 / totalQLevels);

            // Quantize...
            *channelData = val - remainder;

            //// reduce sample rate acording to slider position
            //if (rateDivide > 1) {

            //    if (sample%rateDivide != 0) *channelData = channelData[sample - sample % rateDivide];
            //   
            //}



            // add noise to signal to channel data acording to position of noise knob
            * channelData = *channelData + (noise_signal * paramaterSettings.noise * *channelData);




            // increase signal gain when drive increases, range sets the range of drive that can be applied (max drive val)
            *channelData *= paramaterSettings.drive * paramaterSettings.range;
            // Clips the signal smoothly by passing it though 2/pi * atan(x) (passess close to 1 but never reaches it)
            // wet signal multiplied by blend value and added to dry signal multiplied by 1/blend to balence wet/dry
            // whole signal then multiplied by volume 
       
            *channelData = (((((2.f / juce::float_Pi) * atan(*channelData)) * paramaterSettings.mix) + (cleanSig * (1.f - paramaterSettings.mix))) / 2) * paramaterSettings.volume;
            //*channelData = (((((2.f / juce::float_Pi) * atan(*channelData)) * paramaterSettings.mix) + (cleanSig * (1.f - 1.f))) / 2) * paramaterSettings.volume;


            channelData++;
        }
    }
    waveViewer.pushBuffer(buffer);
    // Pass audio block to dsp
    juce::dsp::AudioBlock<float> block(buffer);


    // Split audio into left and right blocks since DSP can only process mono audio
    auto leftBlock = block.getSingleChannelBlock(0);
    auto rightBlock = block.getSingleChannelBlock(1); 

    juce::dsp::ProcessContextReplacing<float> leftContext(leftBlock);
    juce::dsp::ProcessContextReplacing<float> rightContext(rightBlock);

    // Store samples pre filtering 
    const auto& leftInput = leftContext.getInputBlock();
    const auto& leftOutput = leftContext.getOutputBlock();

    const auto& rightInput = rightContext.getInputBlock();
    const auto& rightOutput = rightContext.getOutputBlock();

    leftMixer.pushDrySamples(leftInput);
    rightMixer.pushDrySamples(rightInput);
    
    leftChain.process(leftContext);
    rightChain.process(rightContext);

    leftMixer.setWetMixProportion(paramaterSettings.mix);
    rightMixer.setWetMixProportion(paramaterSettings.mix);

    leftMixer.mixWetSamples(leftOutput);
    rightMixer.mixWetSamples(rightOutput);


    
    //for (int channel = 0; channel < totalNumInputChannels; ++channel)
    //{
    //    auto* channelData = buffer.getWritePointer (channel);

    //    // ..do something to the data...
    //}
}

//==============================================================================
bool CRUNCH_BOX_2AudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

juce::AudioProcessorEditor* CRUNCH_BOX_2AudioProcessor::createEditor()
{
    return new CRUNCH_BOX_2AudioProcessorEditor (*this);
    //return new juce::GenericAudioProcessorEditor(*this);
}

//==============================================================================
void CRUNCH_BOX_2AudioProcessor::getStateInformation (juce::MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.

    // juce::MemoryOutputStream mos(destData, true);
    // apvts.state.writeToStream(mos);

}

void CRUNCH_BOX_2AudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.

    // auto tree = juce::ValueTree::readFromData(data, sizeInBytes);
    // if (tree.isValid()) {
    //    apvts.replaceState(tree);
        
    //}
}

paramaterSettings getParamaterSettings(juce::AudioProcessorValueTreeState& apvts)
{
    // Get paramater settings from AudioProcessorValueTreeState and put them into the struct "paramaterSettings"

    paramaterSettings settings;

    // Get paramater values
    settings.noise = apvts.getRawParameterValue("NoiseLevel")->load();
    settings.bits = apvts.getRawParameterValue("Bits")->load();
    settings.rate = apvts.getRawParameterValue("Rate")->load();
    settings.reduxMix = apvts.getRawParameterValue("ReduxMix")->load();
    settings.drive = apvts.getRawParameterValue("Drive")->load();
    settings.range = apvts.getRawParameterValue("Range")->load();
    settings.hpf = apvts.getRawParameterValue("HPF")->load();
    settings.lpf = apvts.getRawParameterValue("LPF")->load();
    settings.volume = apvts.getRawParameterValue("Volume")->load();
    settings.mix = apvts.getRawParameterValue("Mix")->load();

    return settings;
}

void CRUNCH_BOX_2AudioProcessor::updateCoefficients(Coefficients& old, const Coefficients& replacements)
{
    *old = *replacements;
}

juce::AudioProcessorValueTreeState::ParameterLayout CRUNCH_BOX_2AudioProcessor::createParameterLayout()
{
    // Create layout containing all paramaters that will be used in plugin
    juce::AudioProcessorValueTreeState::ParameterLayout layout;
    // White Noise Level
    layout.add(std::make_unique<juce::AudioParameterFloat>("NoiseLevel", "NoiseLevel",
        juce::NormalisableRange<float>(0.f, 1.f, 0.01f, 1.f), 0.f));
    // Bit reduction amount
    layout.add(std::make_unique<juce::AudioParameterFloat>("Bits", "Bits",
        juce::NormalisableRange<float>(2.f, 16.f, 1.f, 1.f), 16.f));
    // Sample Rate divider amount
    layout.add(std::make_unique<juce::AudioParameterFloat>("Rate", "Rate",
        juce::NormalisableRange<float>(1.f, 50.f, 1.f, 1.f), 1.f));
    // Bit reduction Mix
    layout.add(std::make_unique<juce::AudioParameterFloat>("ReduxMix", "ReduxMix",
        juce::NormalisableRange<float>(0.f, 1.f, 0.01f, 1.f), 1.f)); // CHANGE TO LOG curve
    // Distortion Drive
    layout.add(std::make_unique<juce::AudioParameterFloat>("Drive", "Drive",
        juce::NormalisableRange<float>(0.f, 1.f, 0.01f, 1.f), 0.f)); // CHANGE TO LOG curve
    // Distortion Range
    layout.add(std::make_unique<juce::AudioParameterFloat>("Range", "Range",
        juce::NormalisableRange<float>(0.f, 1000.f, 0.01f, 1.f), 1.f)); // CHANGE TO LOG curve
    // High Pass Filter 
    layout.add(std::make_unique<juce::AudioParameterFloat>("HPF", "HPF",
        juce::NormalisableRange<float>(20.f, 20000.f, 1.f, 0.3f), 20.f)); 
    // Low Pass Filter 
    layout.add(std::make_unique<juce::AudioParameterFloat>("LPF", "LPF",
        juce::NormalisableRange<float>(20.f, 20000.f, 1.f, 0.3f), 20000.f)); 
    // Output Level
    layout.add(std::make_unique<juce::AudioParameterFloat>("Volume", "Volume",
        juce::NormalisableRange<float>(0.f, 3.f, 0.01f, 1.f), 1.f)); // CHANGE TO LOG curve
    // Dry/Wet
    layout.add(std::make_unique<juce::AudioParameterFloat>("Mix", "Mix",
        juce::NormalisableRange<float>(0.f, 1.f, 0.01f, 1.f), 1.f)); 
    // ADD "AudioParameterChoice" for picking presets or white noise etc (see 27.40 of eq video)


    return layout;
}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new CRUNCH_BOX_2AudioProcessor();
}
