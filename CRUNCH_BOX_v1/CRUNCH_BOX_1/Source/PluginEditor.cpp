/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
CRUNCH_BOX_1AudioProcessorEditor::CRUNCH_BOX_1AudioProcessorEditor (CRUNCH_BOX_1AudioProcessor& p)
    : AudioProcessorEditor (&p), audioProcessor (p)
{

    addAndMakeVisible(noiseKnob = new juce::Slider("Noise"));
    noiseKnob->setSliderStyle(juce::Slider::Rotary);
    noiseKnob->setTextBoxStyle(juce::Slider::NoTextBox, false, 100, 100);

    addAndMakeVisible(bitsKnob = new juce::Slider("Bits"));
    bitsKnob->setSliderStyle(juce::Slider::Rotary);
    bitsKnob->setTextBoxStyle(juce::Slider::TextBoxBelow, false, 200, 20);
    bitsKnob->setRange(2, 16);
    bitsKnob->setValue(16);
    //bitsKnob->addListener(this); MIGHT BE NEEDED

    addAndMakeVisible(rateKnob = new juce::Slider("Rate"));
    rateKnob->setSliderStyle(juce::Slider::Rotary);
    rateKnob->setTextBoxStyle(juce::Slider::TextBoxBelow, false, 200, 20);
    //bitsKnob->setRange(1, 50);
    //rateKnob->setValue(1);

    addAndMakeVisible(driveKnob = new juce::Slider("Drive"));
    driveKnob->setSliderStyle(juce::Slider::Rotary);
    driveKnob->setTextBoxStyle(juce::Slider::NoTextBox, false, 100, 100);

    addAndMakeVisible(rangeKnob = new juce::Slider("Range"));
    rangeKnob->setSliderStyle(juce::Slider::Rotary);
    rangeKnob->setTextBoxStyle(juce::Slider::NoTextBox, false, 100, 100);

    addAndMakeVisible(blendKnob = new juce::Slider("Blend"));
    blendKnob->setSliderStyle(juce::Slider::Rotary);
    blendKnob->setTextBoxStyle(juce::Slider::NoTextBox, false, 100, 100);

    addAndMakeVisible(volumeKnob = new juce::Slider("Volume"));
    volumeKnob->setSliderStyle(juce::Slider::Rotary);
    volumeKnob->setTextBoxStyle(juce::Slider::NoTextBox, false, 100, 100);

    addAndMakeVisible(hpfKnob = new juce::Slider("HPF"));
    hpfKnob->setSliderStyle(juce::Slider::Rotary);
    hpfKnob->setTextBoxStyle(juce::Slider::NoTextBox, false, 100, 100);

    addAndMakeVisible(lpfKnob = new juce::Slider("LPF"));
    lpfKnob->setSliderStyle(juce::Slider::Rotary);
    lpfKnob->setTextBoxStyle(juce::Slider::NoTextBox, false, 100, 100);

    noiseAttachment = new juce::AudioProcessorValueTreeState::SliderAttachment(p.getState(), "noise", *noiseKnob);
    bitsAttachment = new juce::AudioProcessorValueTreeState::SliderAttachment(p.getState(), "bits", *bitsKnob);
    rateAttachment = new juce::AudioProcessorValueTreeState::SliderAttachment(p.getState(), "rate", *rateKnob);
    driveAttachment = new juce::AudioProcessorValueTreeState::SliderAttachment(p.getState(), "drive", *driveKnob);
    rangeAttachment = new juce::AudioProcessorValueTreeState::SliderAttachment(p.getState(), "range", *rangeKnob);
    blendAttachment = new juce::AudioProcessorValueTreeState::SliderAttachment(p.getState(), "blend", *blendKnob);
    volumeAttachment = new juce::AudioProcessorValueTreeState::SliderAttachment(p.getState(), "volume", *volumeKnob);
    hpfAttachment = new juce::AudioProcessorValueTreeState::SliderAttachment(p.getState(), "hpf", *hpfKnob);
    lpfAttachment = new juce::AudioProcessorValueTreeState::SliderAttachment(p.getState(), "lpf", *lpfKnob);



    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (1000, 200);
}

CRUNCH_BOX_1AudioProcessorEditor::~CRUNCH_BOX_1AudioProcessorEditor()
{
}

//==============================================================================
void CRUNCH_BOX_1AudioProcessorEditor::paint (juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId));

    g.setColour (juce::Colours::white);
    g.setFont (15.0f);
    //g.drawFittedText ("Hello World!", getLocalBounds(), juce::Justification::centred, 1);

    g.drawText("Noise", ((getWidth() / 10) * 1) - (100 / 2), (getHeight() / 2) + 5, 100, 100, juce::Justification::centred, false);
    
    g.drawText("Bits", ((getWidth() / 10) * 2) - (100 / 2), (getHeight() / 2) + 5, 100, 100, juce::Justification::centred, false);
    g.drawText("Rate", ((getWidth() / 10) * 3) - (100 / 2), (getHeight() / 2) + 5, 100, 100, juce::Justification::centred, false);
    
    g.drawText("Drive", ((getWidth() / 10) * 4) - (100 / 2), (getHeight() / 2) + 5, 100, 100, juce::Justification::centred, false);
    g.drawText("Range", ((getWidth() / 10) * 5) - (100 / 2), (getHeight() / 2) + 5, 100, 100, juce::Justification::centred, false);
    g.drawText("Blend", ((getWidth() / 10) * 6) - (100 / 2), (getHeight() / 2) + 5, 100, 100, juce::Justification::centred, false);
    g.drawText("Volume", ((getWidth() / 10) * 7) - (100 / 2), (getHeight() / 2) + 5, 100, 100, juce::Justification::centred, false);
    g.drawText("HPF", ((getWidth() / 10) * 8) - (100 / 2), (getHeight() / 2) + 5, 100, 100, juce::Justification::centred, false);
    g.drawText("LPF", ((getWidth() / 10) * 9) - (100 / 2), (getHeight() / 2) + 5, 100, 100, juce::Justification::centred, false);

}

void CRUNCH_BOX_1AudioProcessorEditor::resized()
{
    noiseKnob->setBounds(((getWidth() / 10) * 1) - (100 / 2), (getHeight() / 2) - (100 / 2), 100, 100);
    bitsKnob->setBounds(((getWidth() / 10) * 2) - (100 / 2), (getHeight() / 2) - (100 / 2), 100, 100);
    rateKnob->setBounds(((getWidth() / 10) * 3) - (100 / 2), (getHeight() / 2) - (100 / 2), 100, 100);

    driveKnob->setBounds(((getWidth() / 10) * 4) - (100 / 2), (getHeight() / 2) - (100 / 2), 100, 100);
    rangeKnob->setBounds(((getWidth() / 10) * 5) - (100 / 2), (getHeight() / 2) - (100 / 2), 100, 100);
    blendKnob->setBounds(((getWidth() / 10) * 6) - (100 / 2), (getHeight() / 2) - (100 / 2), 100, 100);
    volumeKnob->setBounds(((getWidth() / 10) * 7) - (100 / 2), (getHeight() / 2) - (100 / 2), 100, 100);
    hpfKnob->setBounds(((getWidth() / 10) * 8) - (100 / 2), (getHeight() / 2) - (100 / 2), 100, 100);
    lpfKnob->setBounds(((getWidth() / 10) * 9) - (100 / 2), (getHeight() / 2) - (100 / 2), 100, 100);

    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..
}
