/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
CRUNCH_BOX_1AudioProcessor::CRUNCH_BOX_1AudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  juce::AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", juce::AudioChannelSet::stereo(), true)
                     #endif
                       )
#endif
{
    state = new juce::AudioProcessorValueTreeState(*this, nullptr);

    state->createAndAddParameter("noise", "Noise", "Noise", juce::NormalisableRange<float>(0.f, 0.5f, 0.0001), 1.0, nullptr, nullptr);
    state->createAndAddParameter("bits", "Bits", "Bits", juce::NormalisableRange<float>(2.f, 16.f, 1.0), 16.0, nullptr, nullptr);
    state->createAndAddParameter("rate", "Rate", "Rate", juce::NormalisableRange<float>(1.f, 50.f, 1.0), 1.0, nullptr, nullptr);
    state->createAndAddParameter("drive", "Drive", "Drive", juce::NormalisableRange<float>(0.f, 1.f, 0.0001), 1.0, nullptr, nullptr);
    state->createAndAddParameter("range", "Range", "Range", juce::NormalisableRange<float>(0.f, 1000.f, 0.0001), 1.0, nullptr, nullptr);
    state->createAndAddParameter("blend", "Blend", "Blend", juce::NormalisableRange<float>(0.f, 1.f, 0.0001), 1.0, nullptr, nullptr);
    state->createAndAddParameter("volume", "Volume", "Volume", juce::NormalisableRange<float>(0.f, 3.f, 0.0001), 1.0, nullptr, nullptr);
    state->createAndAddParameter("hpf", "HPF", "HPF", juce::NormalisableRange<float>(0.f, 20000.f, 0.1), 1.0, nullptr, nullptr);
    state->createAndAddParameter("lpf", "LPF", "LPF", juce::NormalisableRange<float>(0.f, 20000.f, 0.1), 1.0, nullptr, nullptr);



    state->state = juce::ValueTree("noise");
    state->state = juce::ValueTree("bits");
    state->state = juce::ValueTree("rate");
    state->state = juce::ValueTree("drive");
    state->state = juce::ValueTree("range");
    state->state = juce::ValueTree("blend");
    state->state = juce::ValueTree("volume");
    state->state = juce::ValueTree("hpf");
    state->state = juce::ValueTree("lpf");

}

CRUNCH_BOX_1AudioProcessor::~CRUNCH_BOX_1AudioProcessor()
{
}

//==============================================================================
const juce::String CRUNCH_BOX_1AudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool CRUNCH_BOX_1AudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool CRUNCH_BOX_1AudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool CRUNCH_BOX_1AudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double CRUNCH_BOX_1AudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int CRUNCH_BOX_1AudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int CRUNCH_BOX_1AudioProcessor::getCurrentProgram()
{
    return 0;
}

void CRUNCH_BOX_1AudioProcessor::setCurrentProgram (int index)
{
}

const juce::String CRUNCH_BOX_1AudioProcessor::getProgramName (int index)
{
    return {};
}

void CRUNCH_BOX_1AudioProcessor::changeProgramName (int index, const juce::String& newName)
{
}

//==============================================================================
void CRUNCH_BOX_1AudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..

    juce::dsp::ProcessSpec spec;
    spec.maximumBlockSize = samplesPerBlock;
    spec.sampleRate = sampleRate;
    spec.numChannels = getTotalNumOutputChannels();
       
    //LPfilter.prepare(spec);
    filter.prepare(spec);


    reset();
}

void CRUNCH_BOX_1AudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool CRUNCH_BOX_1AudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    juce::ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    // Some plugin hosts, such as certain GarageBand versions, will only
    // load plugins that support stereo bus layouts.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void CRUNCH_BOX_1AudioProcessor::processBlock (juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    juce::ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();

    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels.
    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

    // This is the place where you'd normally do the guts of your plugin's
    // audio processing...
    // Make sure to reset the state if your inner loop is processing
    // the samples and the outer loop is handling the channels.
    // Alternatively, you can process the samples with the channels
    // interleaved by keeping the same state.

    float noise = *state->getRawParameterValue("noise");
    float bits = *state->getRawParameterValue("bits");
    int rate = 4;//static_cast<int> (* state->getRawParameterValue("rate"));
    float drive = *state->getRawParameterValue("drive");
    float range = *state->getRawParameterValue("range");
    float blend = *state->getRawParameterValue("blend");
    float volume = *state->getRawParameterValue("volume");
    float hpf = *state->getRawParameterValue("hpf");
    float lpf = *state->getRawParameterValue("lpf");




    // runs once for mono, twice for stereo 
    for (int channel = 0; channel < totalNumInputChannels; ++channel)
    {
        // stores each audio sample in array of floats "buffer"
        float* channelData = buffer.getWritePointer (channel);

        // for each sample in buffer
        for (int sample = 0; sample < buffer.getNumSamples(); sample++) {

            // generate noise signal
            auto noise_signal = random.nextFloat() * 2.0f - 1.0f;


            // preserve original signal
            float cleanSig = *channelData;

            // reduce bit rate according to slider positions
            float totalQLevels = powf(2, bits); // round bit depth to power of 2
            float val = *channelData;
            float remainder = fmodf(val, 1 / totalQLevels);

            // Quantize...
            *channelData = val - remainder;

            //// reduce sample rate acording to slider position
            //if (rate > 1) {

            //    if (sample%rate != 0) channelData[sample] = channelData[sample - sample % rate];
            //   
            //}



            // add noise to signal to channel data acording to position of noise knob
            *channelData = *channelData + (noise_signal * noise * *channelData);




            // increase signal gain when drive increases, range sets the range of drive that can be applied (max drive val)
            *channelData *= drive * range;
            // Clips the signal smoothly by passing it though 2/pi * atan(x) (passess close to 1 but never reaches it)
            // wet signal multiplied by blend value and added to dry signal multiplied by 1/blend to balence wet/dry
            // whole signal then multiplied by volume 
            *channelData = (((((2.f / juce::float_Pi) * atan(*channelData)) * blend) + (cleanSig * (1.f - blend))) / 2) * volume;
                

            channelData++;
        }
               
    }
    

    // filter POST fx
    filter.setCutoffFrequency(lpf);
    //LPfilter.setCutoffFrequency(hpf);

    auto audioBlock = juce::dsp::AudioBlock<float>(buffer); // copys the buffer to audioBlock
    auto context = juce::dsp::ProcessContextReplacing<float>(audioBlock); // passes buffer to filter (audio gets filtered here)

   filter.process(context);
   // LPfilter.process(context);
}

juce::AudioProcessorValueTreeState& CRUNCH_BOX_1AudioProcessor::getState() {
    return *state;
}

//==============================================================================
bool CRUNCH_BOX_1AudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

juce::AudioProcessorEditor* CRUNCH_BOX_1AudioProcessor::createEditor()
{
    return new CRUNCH_BOX_1AudioProcessorEditor (*this);
}

//==============================================================================
void CRUNCH_BOX_1AudioProcessor::getStateInformation (juce::MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.

    juce::MemoryOutputStream stream(destData, false);
    state->state.writeToStream(stream);
}

void CRUNCH_BOX_1AudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
    juce::ValueTree tree = juce::ValueTree::readFromData(data, sizeInBytes);

    if (tree.isValid()) {
        state->state = tree;

    }
}


//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new CRUNCH_BOX_1AudioProcessor();
}

void CRUNCH_BOX_1AudioProcessor::reset() {
    //LPfilter.reset();
    filter.reset();
}

void CRUNCH_BOX_1AudioProcessor::setType() {
    // tempory solution, should later change to each filter having spesified type
    using fType = juce::dsp::StateVariableTPTFilterType;

    filter.setType(fType::highpass);
       

    switch (filterType)
    {
    case FilterType::LowPass:
        filter.setType(fType::lowpass);
        break;

    case FilterType::BandPass:
        filter.setType(fType::bandpass);
        break;

    case FilterType::HighPass:
        filter.setType(fType::highpass);
        break;

    default:
        filter.setType(fType::lowpass);
        break;
    }
}