/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"

//==============================================================================
/**
*/
class CRUNCH_BOX_1AudioProcessorEditor  : public juce::AudioProcessorEditor
{
public:
    CRUNCH_BOX_1AudioProcessorEditor (CRUNCH_BOX_1AudioProcessor&);
    ~CRUNCH_BOX_1AudioProcessorEditor() override;

    //==============================================================================
    void paint (juce::Graphics&) override;
    void resized() override;

private:
    juce::ScopedPointer<juce::Slider> noiseKnob;
    juce::ScopedPointer<juce::Slider> bitsKnob;
    juce::ScopedPointer<juce::Slider> rateKnob;
    juce::ScopedPointer<juce::Slider> driveKnob;
    juce::ScopedPointer<juce::Slider> rangeKnob;
    juce::ScopedPointer<juce::Slider> blendKnob;
    juce::ScopedPointer<juce::Slider> volumeKnob;
    juce::ScopedPointer<juce::Slider> hpfKnob;
    juce::ScopedPointer<juce::Slider> lpfKnob;

    juce::ScopedPointer < juce::AudioProcessorValueTreeState::SliderAttachment> noiseAttachment;
    juce::ScopedPointer < juce::AudioProcessorValueTreeState::SliderAttachment> bitsAttachment;
    juce::ScopedPointer < juce::AudioProcessorValueTreeState::SliderAttachment> rateAttachment;
    juce::ScopedPointer < juce::AudioProcessorValueTreeState::SliderAttachment> driveAttachment;
    juce::ScopedPointer < juce::AudioProcessorValueTreeState::SliderAttachment> rangeAttachment;
    juce::ScopedPointer < juce::AudioProcessorValueTreeState::SliderAttachment> blendAttachment;
    juce::ScopedPointer < juce::AudioProcessorValueTreeState::SliderAttachment> volumeAttachment;
    juce::ScopedPointer < juce::AudioProcessorValueTreeState::SliderAttachment> hpfAttachment;
    juce::ScopedPointer < juce::AudioProcessorValueTreeState::SliderAttachment> lpfAttachment;

    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    CRUNCH_BOX_1AudioProcessor& audioProcessor;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (CRUNCH_BOX_1AudioProcessorEditor)
};
